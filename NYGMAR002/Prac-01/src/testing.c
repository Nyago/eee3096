#include <stdio.h>
#include <wiringPi.h>

#define LED 17

int main(void){
	printf("Testing program");
	wiringPiSetupGpio();
	pinMode(LED,OUTPUT);
	digitalWrite(LED,HIGH);
	delay(5000);
	digitalWrite(LED,LOW);
	return 0;
}
