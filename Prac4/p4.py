import busio
import digitalio
import board
import adafruit_mcp3xxx.mcp3008 as MCP
from adafruit_mcp3xxx.analog_in import AnalogIn
import threading
import RPi.GPIO as GPIO
import time

btn = 22
chan_ldr = None
chan_temp = None
start_time = 0
ratePosition = 0
rates = [10.0,5.0,1.0]

def setup():

    global chan_ldr
    global chan_temp
    global start_time
    # create the spi bus
    spi = busio.SPI(clock=board.SCK, MISO=board.MISO, MOSI=board.MOSI)

    # create the cs (chip select)
    cs = digitalio.DigitalInOut(board.D5)

    # create the mcp object
    mcp = MCP.MCP3008(spi, cs)

    # create an analog input channel on pin 0
    chan_ldr = AnalogIn(mcp, MCP.P2)
    chan_temp = AnalogIn(mcp, MCP.P1)

    #set up GPIO pins
    GPIO.setup(btn, GPIO.IN)
    GPIO.setup(btn, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.add_event_detect(btn, GPIO.FALLING, callback=btn_pressed, bouncetime=200)
    #set start time
    start_time = time.time()


def fetch_sensor_vals_thread():
    thread = threading.Timer(rates[ratePosition], fetch_sensor_vals_thread)
    thread.daemon = True  # Daemon threads exit when the program does
    thread.start()
    
    current_time = int(time.time()-start_time)

    runtime = str(current_time) + "s"
    temperature = str( round( (chan_temp.voltage - 0.5)/0.01, 2 ) ) + " C"

    print("{:<20}{:<20}{:<20}{:<20}".format( runtime,chan_temp.value,temperature,chan_ldr.value))

def btn_pressed():
    #button interrupt function
    global ratePosition

    ratePosition = (ratePosition + 1)%3


if __name__ == "__main__":
    try:
        setup()

        print("{:<20}{:<20}{:<20}{:<20}".format( "Runtime", "Temp Reading", "Temp", "Light Reading"))

        fetch_sensor_vals_thread()
        while True:
            pass
    except Exception as e:
        print(e)
    finally:
        GPIO.cleanup()